<?php

namespace Cylex\App;

abstract class Driver {

	abstract public function register(array $user);
	
	abstract public function generateUniqueId();
	
	abstract public function getUser($id);
	
	abstract public function resetPassword($id, $current, $new);
	
	abstract public function has($id);
	
	public function visit($name, $password) {
		$id = $this->getUserCaseUserName($name);
		
		if($id) {
			if($this->users[$id]->password === $password) {
				echo "Login cu succes!!!". PHP_EOL;
			}
			else {
				echo "Parola gresita!!!". PHP_EOL;
			}
		}
		else {
			echo 'Ati gresit numele de utilizator!!!'. PHP_EOL;
		}
	}
}