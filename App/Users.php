<?php

namespace Cylex\App;

class Users {
	public $driver;
	public function __construct(Driver $driver) {
		$this->driver = $driver;
	}
	
	public function __call($name, $params){
		if(!method_exists($this->driver, $name)) {
			return false;
		}
		call_user_func_array([$this->driver, $name], $params);
	}
}