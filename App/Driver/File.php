<?php

namespace Cylex\App\Driver;

use Cylex\App\Driver;

class File extends Driver {
	public $time;
	public $value;
	public $users = [];

	public function register(array $user) {
		$id = $this->generateUniqueId();
		$this->users[$id] = new User($user, $id);
	}
	
	public function generateUniqueId() {
		while(1) {
			$id = rand(0, 10000);
			if(!isset($this->users[$id]))
				return $id;
		}
	}
}