<?php

namespace Cylex\App\Driver;

use Cylex\App\Driver;
use Cylex\App\User;

class Memory extends Driver {
	public $time;
	public $value;
	public $users = [];

	public function register(array $user) {
		$id = $this->generateUniqueId();
		$this->users[$id] = new User($user, $id);
	}
	
	public function generateUniqueId() {
		while(1) {
			$id = rand(0, 10000);
			if(!isset($this->users[$id]))
				return $id;
		}
	}
	
	public function getUser($id) {
		if($this->has($id)) {
			return $this->users[$id];
		}
		return false;
	}
	
	public function getUserCaseUserName($name) {
		foreach($this->users as $user) {
			if($user->user_name === $name) {
				return $user->id;
			}
		}
		
		return false;
	}
	
	public function has($id) {
		return isset($this->users[$id]) ? true : false;
	}
	
	public function resetPassword($name, $current, $new) {
		$id = $this->getUserCaseUserName($name);
		
		if($id) {
			if($this->users[$id]->password === $current) {
				$this->users[$id]->reset('password', $new);
			}
			else {
				echo "Ai gresit parola curenta!!!". PHP_EOL;
			}
		}
	}
}