<?php

namespace Cylex\App;

class User {
	public $id = null;
	public $user_name = null;
	public $email = null;
	public $password = null;
	public $access = null;
	private $values = 0;
	/*
	 *Accesss
	 *4 - anonim
	 *3 -
	 *2 - staf (manager)
	 *1 - admin
	 */
	public function __construct(array $user, $id) {
		$this->id = $id;
		$this->setDatas($user);
	}
	
	public function setDatas(array $user) {
		
		foreach($user as $key => $u)
		{
			$this->set($key, $u);
		}
		
		if($this->values == 4) {
			return $this->id;
		}
		else {
			echo "Missing set user data : ",
			(!$this->user_name) ? 'user_name' : '',
			(!$this->email) ? ', email' : '',
			(!$this->password) ? ', password' : '',
			(!$this->access) ? ', access' : '', '!!!', PHP_EOL;
		}
	}
	
	public function set($key, $val) {
		if($this->{$key} === null) {
			$this->{$key} = $val;
			$this->values++;
		}
	}
	
	public function reset($key, $val) {
		$this->{$key} = $val;
	}
}